import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {


  guest: any;
  user: any;

  isname : any;
  about = [
    {
      "id": 0,
       "title": "Vice Chancellor",
      "content":"I have the singular pleasure of welcoming you to the premier and oldest University in Zimbabwe. Our purpose is to provide a quality tertiary education experience that inspires our students to succeed"
    
  },
  {
      "id": 1,
       "title": "Our Mission",
      "content":"To provide high quality and innovative higher education, training, research and services in line with the clients’ needs to enable significant contribution to sustainable development"
    
    },
    {
      "id": 2,
       "title": "Our Vision",
      "content":"To be the leading centre of innovative higher education, research and service provision that is responsive to the developmental needs of Zimbabwe and beyond"
    
    },
    {
      "id": 3,
       "title": "Governance",
      "content":"The University  Council's powers and authority are detailed in the University of Zimbabwe Act Chapetr 25:16 Section 11. Council is the Executive authority of the University. The Chancellor, Vice Chancellor, Pro Vice Chancellors and the Presient of the Students' Union are ex-officio Members of the Council"
    
  },    {
    "id": 4,
     "title": "Our History",
    "content":"The University is renamed University of Zimbabwe after the attainment of independence by Zimbabwe. It was later called the university of Rhodesia"
  
    },
    {
      "id": 5,
       "title": "Strategic Goals",
      "content":"To ensure its effective implementation, a robust results-based management system (RBMS) will be promoted at all levels and through offering appropriate incentives"
    
  }
]
  constructor() {
    const guest = JSON.parse(localStorage.getItem('token1'));
    this.guest = guest;

    const user = JSON.parse(localStorage.getItem('token'));
    this.user = user;
   }

  ngOnInit() {
  }
  
  name(id: any) {
    this.isname = id;

  }
}
