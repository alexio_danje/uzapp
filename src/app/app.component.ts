import { DrugService } from './services/drug.service';
  import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';

import { OfflineManagerService } from './services/offline-manager.service';
import { NetworkService, ConnectionStatus } from './services/network.service';
import { HomeService } from './services/home.service';
import { EventsService } from './services/events.service';
import { MapService } from './services/map.service';
import { DirectoryService } from './services/directory.service';
import { HostelService } from './services/hostel.service';
import { UserService } from './services/user.service';
import { LaptopService } from './services/laptop.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit{

  home: any;
  tip: any;
  event: any;
  faq: any;
  place: any;
  faculty: any;
  security: any;
  id: any;
  token: any;

  public appPages = [

    {
      title: 'News',
      url: '/newslist',
      icon: 'home'
    },
 
    {
      title: 'About',
      url: '/about',
      icon: 'information-circle'
    },
    {
      title: 'Phone Directory',
      url: '/directory',
      icon: 'call'
    },
    {
      title: 'Clinic',
      url: '/clinic',
      icon: 'medkit'
    },
    {
      title: 'Security',
      url: '/security',
      icon: 'construct'
    }
    ,
    {
      title: 'FAQs',
      url: '/faqs',
      icon: 'help'
    }
  ];
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router:Router,
    private offlineManager: OfflineManagerService,
    private networkService: NetworkService,


  ) {
    const token = JSON.parse(localStorage.getItem('token'));
    this.token = token;
    
  }
  ngOnInit() {
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      // this.placelist(true);
      // this.tiplist(true);
      // this.eventlist(true);
      // this.faqlist(true);
      // this.facultylist(true);
      // this.securityNumbers(true);
      // this.departmentlist(true);
      // this.phonelist(true);
      // // this.hostellist(true);

      // // this.janitorlist(true);
      // // this.wardenlist(true);
      // // this.commitielist(true);


      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log('we are online, run checkForEvents...');
          this.offlineManager.checkForEvents().subscribe();
        }
      })
    });
  }

  backToWelcome(){
    this.router.navigateByUrl('login');    

}

  logout(){
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 500);
}

}
