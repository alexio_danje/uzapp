import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { LoadingService } from './services/loading.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Network } from '@ionic-native/network/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { GeneralinfoPageModule } from './campusservices/security/generalinfo/generalinfo.module';
import { ModalpagePageModule } from './campusservices/security/generalinfo/modalpage/modalpage.module';
import { CrimedetailPageModule } from './campusservices/security/crimeprevention/crimedetail/crimedetail.module';
import { CrimepreventionPageModule } from './campusservices/security/crimeprevention/crimeprevention.module';
import { TipdetailPageModule } from './clinic/tipdetail/tipdetail.module';
import { NoticedetailPageModule } from './notices/noticedetail/noticedetail.module';
import { EventdetailPageModule } from './events/eventdetail/eventdetail.module';
import { AuthModule } from './auth/auth.module';
import { DrugdetailPageModule } from './clinic/drugdetail/drugdetail.module';
import { LaunchNavigator } from '@ionic-native/launch-navigator/ngx';


@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule,
    GeneralinfoPageModule,
    ModalpagePageModule,
    DrugdetailPageModule,
    TipdetailPageModule,
    NoticedetailPageModule,
    EventdetailPageModule,
    AuthModule,
    CrimedetailPageModule,
    CrimepreventionPageModule],
  providers: [
    StatusBar,
    SplashScreen,
    Network,
    LoadingService,
    Geolocation, 
    LaunchNavigator,
    CallNumber,
    SocialSharing,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
