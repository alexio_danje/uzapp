import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthService } from '../auth.service';
import { LoadingService } from '../../services/loading.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';

@Component({
  selector: 'app-guest',
  templateUrl: './guest.page.html',
  styleUrls: ['./guest.page.scss'],
})
export class GuestPage implements OnInit {

  userData: any;
  lat: any
  lng: any;
  user: any;
  contact = "";
  name = "";
  guest: any;

  constructor(private  authService:  AuthService, 
              private  router:  Router,
              public alertController: AlertController,
              private geolocation: Geolocation,) { 
                // const guest = JSON.parse(localStorage.getItem('token1'));
                // this.guest = guest;

                this.check();
              }

  ngOnInit() {
  }

  check(){
    if(this.guest != null){
      this.router.navigateByUrl('tebs');
    }
  }

  presentAlert1() {
    this.alertController.create({
    header: 'Fill in All fields',
    message: 'Enter Valid email or phone number',
    // subHeader: '',
    buttons: ['Dismiss']}).then(alert=> alert.present());
  }

  presentAlert2() {
    this.alertController.create({
    header: 'Error !!!!!!',
    message: 'pliz check your details or network and try again',
    subHeader: 'Something went wrong !!!',
    buttons: ['Dismiss']}).then(alert=> alert.present());
  }


  other(){
    this.router.navigateByUrl('login');
  }

  login(){
    // this.router.navigateByUrl('register');
    if(this.contact == "" || this.name == ""){
      this.presentAlert1();
    }

    else{
      


    this.geolocation.getCurrentPosition().then((resp) => {
      console.log('latitude', resp.coords.latitude);
      console.log('longitude', resp.coords.longitude);
    
     
    


    this.user = {
                "id":"",
                "name": this.name,
                "phone": this.contact,
                "lng": resp.coords.longitude,
                "lat": resp.coords.latitude, 
            };

         

            this.authService.guest(this.user, '').subscribe((res)=>{
              this.userData = res;
              console.log('data', this.user);
              // localStorage.setItem('token1', JSON.stringify(this.userData));
              // console.log('token1', this.userData);
              this.router.navigateByUrl('tebs');
            }, (err) => {
              // this.alert();
              this.presentAlert2();
          });


  }).catch((error) => {
    // this.loading.dismiss();
    console.log('Error getting location', error);
  });
    }

  }

}
