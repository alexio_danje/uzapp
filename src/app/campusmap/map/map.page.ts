import { LoadingService } from './../../services/loading.service';
import { Router } from '@angular/router';
import { Platform } from '@ionic/angular';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { GeolocationOptions, Geoposition, Geolocation } from '@ionic-native/geolocation/ngx';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';

import { MapService } from '../../services/map.service';

declare var google;


@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage implements OnInit {

  options : GeolocationOptions;
  currentPos : Geoposition;

  searchTerm = "";
  searching: any = false;

  places = [];
  distance = [];

  latitude :any;
  longitude: any;

  place: any;
 

  @ViewChild('directionsPanel', {static: false}) directionsPanel: ElementRef;
  @ViewChild('map', {static: false}) mapElement: ElementRef;
  map: any;


  constructor(public loading: LoadingService,
              private geolocation: Geolocation,
              private dataService: NagivateDataService,
              private mapService: MapService,
              private plt: Platform,
              private router: Router,) { }

  
              ngOnInit() {
                this.plt.ready().then(() => {
                  this.loadData();
                })
                this.setFilteredItems();
              }
            
              loadData() {
                this.mapService.getOfflineCords().subscribe(res => {
                  this.places = res;
                  console.log(this.places)
                  this. initMap();
                })
              }

           
              Detail(place, lat, lng){
                
                  this.dataService.setParamData(this.places);
                  this.dataService.setParamData1(lat);
                  this.dataService.setParamData2(lng);
                  this.dataService.setParamData3(place);
                  this.router.navigateByUrl('placeroute');  
              } 


    
              initMap(){
                // console.log('map again', this.places);

                  this.geolocation.getCurrentPosition().then((resp)=>{
                  let coords = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
                  this.latitude = resp.coords.latitude;
                  this.longitude = resp.coords.longitude
            
                  let mapOptions = {
                    center:coords,
                    zoom:17,
                    mapTypeId:google.maps.MapTypeId.ROADMAP,
                    disableDefaultUI:true,
            
                  }

                  for(let i = 0; i < this.places.length; i++){
                
                    let p = 0.017453292519943295;    // Math.PI / 180
                    let c = Math.cos;
                    let a = 0.5 - c((resp.coords.latitude-parseFloat(this.places[i].lat)) * p) / 2 + c(parseFloat(this.places[i].lat) * p) *c((resp.coords.longitude) * p) * (1 - c(((resp.coords.longitude- parseFloat(this.places[i].lng)) * p))) / 2;
                    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
                    
                    this.distance[i] = (dis).toFixed(2);
                    
                  }
            
                  //Creating an instance of the map
                  this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
                  // Setting a marker on the map for Current location
                  let marker = new google.maps.Marker({
                    map:this.map,
                    position: coords,
                    title:"You",
                    animation:google.maps.Animation.DROP,
                    enableHighAccuracy:true,
                  })
            
                  //Listening to the click event from the marker.
                  marker.addListener('click', toggleBounce);
                  function toggleBounce(){
                    if(marker.getAnimation() !== null){
                      marker.setAnimation(null);
                    }else{
                      marker.setAnimation(google.maps.Animation.DROP);
                    }
                  }
            
                  // Markers of places
                  for( let places of this.places){
                    let coords2 = new google.maps.LatLng(parseFloat(places.lat), parseFloat(places.lng));
                    // console.log(new google.maps.LatLng(parseFloat(places.lat), parseFloat(places.lng)));
                    let marker2 = new google.maps.Marker({
                      map:this.map,
                      position: coords2,
                      clickable:true,
                      title:places.location,
                      label:places.name,
                      icon:"../../../assets/imgs/map/"+places.icon+".png",
                      animation:google.maps.Animation.DROP,
                    });
                    // console.log('icons', this.guests)
                    marker2.addListener('click', openPage =>{
                    
                      
                      this.dataService.setParamData(this.places);
                      this.dataService.setParamData1(places.lat);
                      this.dataService.setParamData2(places.lng);
                      this.dataService.setParamData3(places);
                    
                      this.router.navigateByUrl('placeroute');  
                    });
                 
                   }

                  }).catch((error)=>{
                    console.log(error);
                  })

            }


            filterItems(searchTerm) {
              // console.log("NAME", this.searchTerm);
              // console.log("array",this.places)
              
              return this.places.filter(item => {
                return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
              });
            }

            medpharm1 = [];

            setFilteredItems() {

              this.place = this.filterItems(this.searchTerm.toLowerCase());
              // console.log('place', this.place);
              

              this.geolocation.getCurrentPosition().then((resp)=>{

                // let coords = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);

                for(let i = 0; i < this.place.length; i++){

                  this.medpharm1[i] = this.place[i];

                  let p = 0.017453292519943295;    // Math.PI / 180
                  let c = Math.cos;
                  let a = 0.5 - c((resp.coords.latitude-parseFloat(this.medpharm1[i].lat)) * p) / 2 + c(parseFloat(this.medpharm1[i].lat) * p) *c((resp.coords.longitude) * p) * (1 - c(((resp.coords.longitude- parseFloat(this.medpharm1[i].lng)) * p))) / 2;
                  let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
                  
                  this.distance[i] = (dis).toFixed(2);
                }
              });
            }

}
