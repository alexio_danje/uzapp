import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceroutePage } from './placeroute.page';

describe('PlaceroutePage', () => {
  let component: PlaceroutePage;
  let fixture: ComponentFixture<PlaceroutePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceroutePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceroutePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
