// import { LaunchNavigator } from './../../../../plugins/uk.co.workingedge.phonegap.plugin.launchnavigator/uk.co.workingedge.phonegap.plugin.launchnavigator.d';
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator/ngx';
import { Router } from '@angular/router';
import { AlertController, ToastController } from '@ionic/angular';
import { LoadingService } from './../../services/loading.service';
import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { GeolocationOptions, Geoposition, Geolocation } from '@ionic-native/geolocation/ngx';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';

declare var google;

@Component({
  selector: 'app-placeroute',
  templateUrl: './placeroute.page.html',
  styleUrls: ['./placeroute.page.scss'],
})
export class PlaceroutePage implements OnInit {

  options : GeolocationOptions;
  currentPos : Geoposition;
  current : any;

  @ViewChild('map2', {static: false}) mapElement2: ElementRef;
  @ViewChild('directionsPanel', {static: false}) directionsPanel: ElementRef;

  map: any;

  response :any;
  latitude: any;
  longitude: any;
  lati: any;
  longi: any;
  full: any
  test = "Test";
  distance: any;

  constructor(public geolocation : Geolocation,
    private launchNavigator: LaunchNavigator,
    public loading: LoadingService,
    public alertController: AlertController,
    public toastCtrl:ToastController,
    public dataService:NagivateDataService,
    private router: Router,
    ) {
      // console.log('lat', this.dataService.getParamData1())
      // console.log('lng', this.dataService.getParamData2())
      // console.log('places', this.dataService.getParamData3())
     }

  ngOnInit() {
    let paramData = this.dataService.getParamData3()
    this.response = paramData;
    console.log(paramData)
    this.loadMap1();
    this.startNavigation1();

    this.geolocation.getCurrentPosition().then((resp)=>{
      // let coords = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      this.latitude = resp.coords.latitude;
      this.longitude = resp.coords.longitude;

      // console.log('lat', this.latitude, 'lng', this.longitude);
    });
  }

  getDirections(lat, lng){
    // console.log(latitude + " " + longitude)
    // console.log('lat', this.latitude, 'lng', this.longitude);

    let options: LaunchNavigatorOptions = {
      start: [this.latitude, this.longitude],
      app: this.launchNavigator.APP.GOOGLE_MAPS,
    }
    this.launchNavigator.navigate([lat, lng], options).then(() => {
      console.log('launch was succesful')
    }).catch((error) => {
      console.log(error)
    })
  }

  presentAlert2() {
    const alert = this.alertController.create({
    header: 'Error',
    message: 'Please try again later',
    subHeader: 'Something went wrong!!!!',
    buttons: ['Dismiss']}).then(alert=> alert.present());
  }


  loadMap1(){ 

    this.geolocation.getCurrentPosition().then((resp)=>{

      let coords = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
      let mapOptions = {
        center: coords,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
      this.map = new  google.maps.Map(this.mapElement2.nativeElement, mapOptions);

       // Markers of places
       for(let places of this.dataService.getParamData()){
        let coords2 = new google.maps.LatLng(parseFloat(places.lat), parseFloat(places.lng));
        // console.log(new google.maps.LatLng(parseFloat(places.lat), parseFloat(places.lng)));
        let marker2 = new google.maps.Marker({
          map:this.map,
          position: coords2,
          clickable:true,
          title:places.location,
          label:places.name,
          icon:"../../../assets/imgs/map/"+places.icon+".png",
          animation:google.maps.Animation.DROP,
        });
        // console.log('icons', this.guests)
        marker2.addListener('click', openPage =>{
        
          
          this.dataService.setParamData(places);
          this.dataService.setParamData1(places.lat);
          this.dataService.setParamData2(places.lng);
        
          this.router.navigateByUrl('placeroute');  
        });

     
       }
    });
  }


startNavigation1(){
   
  this.loading.presentLoading();

   this.geolocation.getCurrentPosition().then((resp)=> {
    let coords = new google.maps.LatLng(resp.coords.latitude, resp.coords.longitude);
    let directionsService = new google.maps.DirectionsService;
    let directionsDisplay = new google.maps.DirectionsRenderer;


    let p = 0.017453292519943295;    // Math.PI / 180
    let c = Math.cos;
    let a = 0.5 - c((resp.coords.latitude-parseFloat(this.dataService.getParamData1())) * p) / 2 + c(parseFloat(this.dataService.getParamData1()) * p) *c((resp.coords.longitude) * p) * (1 - c(((resp.coords.longitude- parseFloat(this.dataService.getParamData2())) * p))) / 2;
    let dis = (12742 * Math.asin(Math.sqrt(a))); // 2 * R; R = 6371 km
    
    this.distance = (dis).toFixed(2);
 
    // directionsDisplay.setPanel(this.directionsPanel.nativeElement);
    this.map = new  google.maps.Map(this.mapElement2.nativeElement, {
      center: coords,
      zoom:20,
      mapTypeId:google.maps.MapTypeId.ROADMAP ,
      disableDefaultUI:true,  
    });

    directionsDisplay.setMap(this.map);

    directionsService.route({
          origin: {lat:resp.coords.latitude , lng:resp.coords.longitude},
          destination:{lat: parseFloat(this.dataService.getParamData1()), lng: parseFloat(this.dataService.getParamData2())},
          // travelMode:google.maps.TravelMode['DRIVING']
          travelMode:google.maps.TravelMode['WALKING']
       }, (res, status) => {

        if(status == google.maps.DirectionsStatus.OK){
              directionsDisplay.setMap(this.map);
              directionsDisplay.setDirections(res);
              this.loading.dismiss();
            }else{
              this.loading.dismiss();
              this.map = ""
              this.presentAlert2();
            }

       });

    });
  }


}
