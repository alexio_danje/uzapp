import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DirectoryService } from 'src/app/services/directory.service';
import { Platform, NavController, ToastController } from '@ionic/angular';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';

@Component({
  selector: 'app-department',
  templateUrl: './department.page.html',
  styleUrls: ['./department.page.scss'],
})
export class DepartmentPage implements OnInit {

  response: any;
  faculty: any;
  phones = [];
  numbers: any;
  searchTerm = "";
  filtered: any;
  department: any;
  faculty_id: any;
  dpt_list = [];
  dpt_list_all = [];

  constructor(private directoryService: DirectoryService,
    private navigateData: NagivateDataService,
    private router: Router,) { }

  ngOnInit() {
    // let paramData = this.navigateData.getParamData();
    // this.response = paramData;
    // console.log(this.response); 

    // this.faculty = this.response[0]['faculty']['fullname'];
    //  console.log(this.faculty);
    // this.setFilteredItems();

    //new stuff
    this.setFilteredItems();
  
    this.departmentList(this.navigateData.getParamData1());
   
  }

  departmentList(faculty_id){
    this.directoryService.getDepartmentsOffline1().subscribe(res => {  
      this.dpt_list = res;
    
      for(let i=0; i<this.dpt_list.length; i++){
        if(this.dpt_list[i]['faculty']['id'] == faculty_id){
          this.dpt_list_all.push(this.dpt_list[i]);
          this.department = this.dpt_list[i]['faculty']['fullname'];
        }
       
      }
      console.log('faculty department', this.dpt_list_all);
    })
  }

  departmentDetails1(department_id){
    this.navigateData.setParamData1(department_id);
    this.router.navigateByUrl('personels'); 
  }

  hey = "leo"
  departmentDetails(id, data) {
    data = this.hey;
    this.directoryService.getNumbers(id,data).subscribe(res => {  
      this.numbers = res; 
      console.log(this.numbers)
      this.navigateData.setParamData(this.numbers);
      this.router.navigateByUrl('personels');  
    })
  }
  filterItems(searchTerm) {
    
    return this.dpt_list_all.filter(item => {
      return item.fullname.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  setFilteredItems() {
    this.filtered = this.filterItems(this.searchTerm);
  }

}
