import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonelsPage } from './personels.page';

describe('PersonelsPage', () => {
  let component: PersonelsPage;
  let fixture: ComponentFixture<PersonelsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonelsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonelsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
