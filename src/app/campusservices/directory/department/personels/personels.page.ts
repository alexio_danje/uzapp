import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DirectoryService } from 'src/app/services/directory.service';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-personels',
  templateUrl: './personels.page.html',
  styleUrls: ['./personels.page.scss'],
})
export class PersonelsPage implements OnInit {

  response: any;
  department: any;
  isname: any;
  departmentname: any;

  phone_list = [];
  phone_list_all = [];

  constructor(public router:Router, 
              public activatedRoute:ActivatedRoute,
    public directoryService: DirectoryService,
    private navigateData: NagivateDataService,
    private callNumber: CallNumber,
    private socialSharing: SocialSharing,
              ) { }

  ngOnInit() {
    // let paramData = this.navigateData.getParamData()
    // this.response = paramData;
    // console.log("Response is",this.response)
    //this.department = this.response[0]['department']['fullname'];
    console.log('department_id', this.navigateData.getParamData1());

    this.phoneList(this.navigateData.getParamData1());
    }

    phoneList(department_id){
      this.directoryService.getNumbersOffline1().subscribe(res => {  
        this.phone_list = res;
        console.log('phone list', this.phone_list);
        
        for(let i=0; i<this.phone_list.length; i++){
          if(this.phone_list[i]['department']['id'] == department_id){
            this.phone_list_all.push(this.phone_list[i]);
            this.departmentname = this.phone_list[i]['department']['fullname'];
          }
        }
        console.log('department numbers', this.phone_list_all);
      })
    }

    callNow(number) {
      this.callNumber.callNumber(number, true)
        .then(res => console.log('Launched dialer!', res))
        .catch(err => console.log('Error launching dialer', err));
    }

  share(title) {
    // 'http://uzbuddie.pythonanywhere.com'
    
    this.socialSharing.share(title).then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });


  }
    
  name(id: any) {
    this.isname = id;

  }

}
