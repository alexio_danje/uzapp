import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DirectoryService } from 'src/app/services/directory.service';
import { NagivateDataService } from './../../services/nagivate-data.service';
import { Platform, NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-directory',
  templateUrl: './directory.page.html',
  styleUrls: ['./directory.page.scss'],
})
export class DirectoryPage implements OnInit {

  faculties = [];
  faculty: any;
  searchTerm = "";
  department: any;

  constructor(private directoryService: DirectoryService,
    private navigateData: NagivateDataService,
    private plt: Platform,
    private navCtrl: NavController,
    private router: Router,
    private toastController:ToastController,) { }

  ngOnInit() {
    this.plt.ready().then(() => {
      this.loadData();
    })
    this.setFilteredItems();
  }

  loadData() {
    this.directoryService.getFacultiesOffline().subscribe(res => {
      this.faculties = res;
      console.log(this.faculties)

    });
  }

  hey = "leo"
  departments(id, data){
    data = this.hey;
    this.directoryService.getDepartments(id, data).subscribe(res => {
      this.departments = res; 

      this.navigateData.setParamData(this.departments);
      this.router.navigateByUrl('department');  
    })
  }

  departments1(faculty_id){
    this.navigateData.setParamData1(faculty_id);
    this.router.navigateByUrl('department'); 
  }



  filterItems(searchTerm) {
    
    return this.faculties.filter(item => {
      return item.shortname.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  setFilteredItems() {
    this.faculty = this.filterItems(this.searchTerm);
  }

}
