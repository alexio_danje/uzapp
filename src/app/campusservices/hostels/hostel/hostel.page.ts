import { HostelService } from './../../../services/hostel.service';
import { Component, OnInit } from '@angular/core';
import { NagivateDataService } from '../../../services/nagivate-data.service';

@Component({
  selector: 'app-hostel',
  templateUrl: './hostel.page.html',
  styleUrls: ['./hostel.page.scss'],
})
export class HostelPage implements OnInit {

  response: any;
  hostel: any;
  hostel_id: any;

  janitors = [];
  wardens = [];
  commities = [];

  njanitors = [];
  nwardens = [];
  ncommities = [];

  constructor(
    private navigateData: NagivateDataService,
    private hostelService: HostelService, 
              ) { }

  ngOnInit() {
    console.log('id iyo', this.navigateData.getParamData1());
    let paramData = this.navigateData.getParamData()
    this.response = paramData;
    console.log("Response is",this.response)
    this.hostel = this.response['name'];

    //mudhara code
    this.hostel_id = this.response['id'];
    console.log('hostel id', this.hostel_id);

    

    this.getVanhu();
 

    

    }

    getVanhu(){
      this.hostelService.getJanitorsOffline().subscribe(res => {
        console.log('janitors', res);
        this.janitors = res;
      });
  
      this.hostelService.getWardensOffline().subscribe(res => {
        console.log('wardens', res);
        this.wardens = res;
      });
  
      this.hostelService.getCommitiesOffline().subscribe(res => {
        console.log('commities', res);
        this.commities = res; 
        
        this.getAll();
    
      });
    }

    getAll(){

      for(let i=0; i < this.janitors.length; i++){
        if(this.janitors[i]['hostel']['id'] ==  this.navigateData.getParamData1())
        {
          this.njanitors.push(this.janitors[i]);
        }

      }

      for(let i=0; i < this.wardens.length; i++){
        if(this.wardens[i]['hostel']['id'] ==  this.navigateData.getParamData1())
        {
          this.nwardens.push(this.wardens[i]);
        }
      }

      for(let i=0; i < this.commities.length; i++){
        if(this.commities[i]['hostel']['id'] ==  this.navigateData.getParamData1())
        {
          this.ncommities.push(this.commities[i]);
        }
      }

      console.log('new janitors', this.njanitors);
      console.log('new wardens', this.nwardens);
      console.log('new commities', this.ncommities);
    }

}
