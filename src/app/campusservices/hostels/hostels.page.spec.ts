import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HostelsPage } from './hostels.page';

describe('HostelsPage', () => {
  let component: HostelsPage;
  let fixture: ComponentFixture<HostelsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HostelsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HostelsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
