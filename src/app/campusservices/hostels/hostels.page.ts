import { Component, OnInit } from '@angular/core';
import { Platform, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { HostelService } from '../../services/hostel.service';
import { NagivateDataService } from '../../services/nagivate-data.service';
@Component({
  selector: 'app-hostels',
  templateUrl: './hostels.page.html',
  styleUrls: ['./hostels.page.scss'],
})
export class HostelsPage implements OnInit {

  hostels = [];
  hostel = [];

  news: any;
  searchTerm = "";
  residence: any;

  constructor(private hostelService: HostelService, 
              private plt: Platform,
              private navCtrl: NavController,
              private router: Router,
    private toastController: ToastController,
    private navigateData: NagivateDataService,
             ) { }

  ngOnInit() {
    this.plt.ready().then(() => {
      this.loadData();
    })
    this.setFilteredItems();
  }

  loadData() { 
    this.hostelService.getHostelsOffline().subscribe(res => {
      console.log('hostels', res);
      this.hostels = res;
    });
  }


  Hostel(val, id) {
    console.log('log', id);
    
        this.navigateData.setParamData(val);
        this.navigateData.setParamData1(id);
        this.router.navigateByUrl('hostel');  
      

   }


  filterItems(searchTerm) {
    // console.log("NAME", this.searchTerm);
    // console.log("array",this.hostels)
    
    return this.hostels.filter(item => {
      return item.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
    });
  }
  setFilteredItems() {
    this.hostel = this.filterItems(this.searchTerm);
  }
}
