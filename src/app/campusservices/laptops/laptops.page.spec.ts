import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LaptopsPage } from './laptops.page';

describe('LaptopsPage', () => {
  let component: LaptopsPage;
  let fixture: ComponentFixture<LaptopsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LaptopsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LaptopsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
