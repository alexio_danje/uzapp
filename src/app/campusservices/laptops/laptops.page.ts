import { Component, OnInit } from '@angular/core';
import { DirectoryService } from 'src/app/services/directory.service';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';
import { LaptopService } from 'src/app/services/laptop.service';
import { Platform } from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-laptops',
  templateUrl: './laptops.page.html',
  styleUrls: ['./laptops.page.scss'],
})
export class LaptopsPage implements OnInit {
  laptop: any;
  id: any;
  token: any;
  users: any;

  constructor(private laptopService: LaptopService,
    private userService: UserService,
  
    private plt: Platform,
) {



  }

  ngOnInit() {
 
    this.plt.ready().then(() => {
      this.loadData();
     })
  }

  
  loadData() {
    this.laptopService.getLaptopOffline().subscribe(res => {
      this.laptop = res;
      console.log(this.laptop)

    });
  }

}
