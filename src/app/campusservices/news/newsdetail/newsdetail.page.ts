import { Component, OnInit } from '@angular/core';

import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { NagivateDataService } from '../../../services/nagivate-data.service';


@Component({
  selector: 'app-newsdetail',
  templateUrl: './newsdetail.page.html',
  styleUrls: ['./newsdetail.page.scss'],
})
export class NewsdetailPage implements OnInit {

  news: any;

  constructor(public navData: NagivateDataService,
    private socialSharing: SocialSharing, ) { }

  ngOnInit() {
    this.news = this.navData.getParamData();
    console.log('news', this.news);
  }

     // share(message, subject, file, url)
     share(title,content,image, link_url){

      console.log('http://uzbuddie.pythonanywhere.com',image);
      // 'http://uzbuddie.pythonanywhere.com'
    
      this.socialSharing.share(title + content , content, "http://uzbuddie.pythonanywhere.com"+image, " Read more on "+link_url).then(() => {
        console.log("shareSheetShare: Success");
      }).catch(() => {
        console.error("shareSheetShare: failed");
      });


  }

}
