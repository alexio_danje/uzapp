import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';
import { DirectoryService } from 'src/app/services/directory.service';
import { Platform } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-contactsecurity',
  templateUrl: './contactsecurity.page.html',
  styleUrls: ['./contactsecurity.page.scss'],
})
export class ContactsecurityPage implements OnInit {
  numbers = [];
  searchTerm = "";
  filtered: any;
  id: any;
  isname : any;
  constructor(private directoryService: DirectoryService,
    private navigateData: NagivateDataService,
    private plt: Platform,
    private router: Router,
    private callNumber: CallNumber,
    private socialSharing: SocialSharing,) {
  
     }

  ngOnInit() {
    this.plt.ready().then(() => {
      this.contact();
    })
    this.setFilteredItems();
  }


  contact() {
 
    this.directoryService.getSecurityOffline().subscribe(res => { 
      this.numbers = res; 
      this.navigateData.setParamData(this.numbers);
      
    })
  }
  contacts() {
    this.router.navigateByUrl('callsecurity');
  }
  filterItems(searchTerm) {
    
    return this.numbers.filter(item => {
      return item.shortname.toLowerCase().indexOf(searchTerm) > -1;
    });
  }
  setFilteredItems() {
    this.filtered = this.filterItems(this.searchTerm);
  }

  callNow(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }
share(title) {
  // 'http://uzbuddie.pythonanywhere.com'
  
  this.socialSharing.share(title).then(() => {
    console.log("shareSheetShare: Success");
  }).catch(() => { 
    console.error("shareSheetShare: failed");
  });


}
  
name(id: any) {
  this.isname = id;

}
}
