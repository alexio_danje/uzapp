import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrimedetailPage } from './crimedetail.page';

describe('CrimedetailPage', () => {
  let component: CrimedetailPage;
  let fixture: ComponentFixture<CrimedetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrimedetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrimedetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
