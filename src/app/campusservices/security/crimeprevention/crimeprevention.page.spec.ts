import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrimepreventionPage } from './crimeprevention.page';

describe('CrimepreventionPage', () => {
  let component: CrimepreventionPage;
  let fixture: ComponentFixture<CrimepreventionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrimepreventionPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrimepreventionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
