import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CrimedetailPage } from './crimedetail/crimedetail.page';

@Component({
  selector: 'app-crimeprevention',
  templateUrl: './crimeprevention.page.html',
  styleUrls: ['./crimeprevention.page.scss'],
})
export class CrimepreventionPage implements OnInit {

  constructor(public modalController: ModalController) {

  }
  infoList: any[] = [{
    title: "Active Participation",
    content:"Every member of the community is expected to participate in crime prevention. Preventing crime is a team effort. Proactive crime prevention and security consciousness help to ensure a safe and secure environment enabling work and study to take place with little or no disruption. Staff and students should play a part in making every conceivable effort to counter the threat of crime"
  },
    {
      title: "Incident Reporting",
      content: "It is the responsibility of all staff and students to report all activity, real or suspected, minor or serious, of a suspicious or criminal nature. This is because incident reporting is crucial to the identification of a crime. It facilitates investigations and recommendations to be made to prevent or minimize the chances of recurrence. Comprehensive reporting of incidents provides an accurate picture and assessment of the level and patterns of crime on campus and helps the Security Department to ensure that adequate resources and measures are provided or put in place to prevent or minimize crime. Success in the Security Department’s fight against crime is greatly enhanced by fast and detailed reporting"
    },
    {
      title: "Functions, Meetings, Special Events",
      content:"Departments must inform the Security Department, giving at least 5 working days notice, of any important meetings, events and functions, especially when non- university persons or V.I.Ps are attending and which are to be held on campus"
    },
    {
      title: "Security and Safety Tips",
      content:"Keep doors locked when away from your office, room or work station. Lock away valuables securely.Report presence of strangers and suspicious behaviour. Get to know your neighbour and share information about suspicious activities.Avoid poorly lit secluded areas. If this is unavoidable walk in the company of another person."
    }
  ];
  ngOnInit() {
  }
  async presentModal(info) {
    const modal = await this.modalController.create({
      component: CrimedetailPage,
      componentProps: { info: info }
    });
    // modal.onDidDismiss().then(data=>{
    //   this.infoList[index]=data.data
    // })
    return await modal.present();
  }

}
