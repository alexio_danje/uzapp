import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ModalpagePage } from './modalpage/modalpage.page';

@Component({
  selector: 'app-generalinfo',
  templateUrl: './generalinfo.page.html',
  styleUrls: ['./generalinfo.page.scss'],
})
export class GeneralinfoPage implements OnInit {

  constructor(public modalController: ModalController) {

  }
  infoList: any[] = [{
    title: "Welcome Remaks",
    content:"It is with great pleasure that I welcome you to the University of Zimbabwe Security Department website. Prior to 1994, the university used to outsource the security function to private security companies. There was no security department on campus and the private security companies reported to a university official who had no security background. As the university continued to develop and acquire state of the art equipment and the student and staff population increased, the need to employ trained and experienced security professionals was recognized and acknowledged at the highest levels. The idea of forming a department with the sole responsibility for the security function was discussed and approved. This saw the birth of the Security department. A small team of officers was then recruited and began the crucial task of forming a fully fledged department. Since then the department has undergone phenomenal growth. Security operational systems were put in place and now the department plays a critical role in the entire operations of the university. Crime levels on the campus are relatively low as compared to the situation obtaining in other areas in the capital city, Harare. The department has a dedicated team of officers, sergeants and security guards fully committed to ensuring the security and safety wellbeing of staff, students and visitors as well as property on campus.\n\nThe University of Zimbabwe is a community of over 12 000 people, living, working and studying together on campus. As with any community of this magnitude, public safety and security as well as crime awareness and prevention are concerns. Whilst it is our responsibility to deal with these concerns, we believe that the security of our campus is also a responsibility shared by all members of the community. Security awareness and crime prevention are ways of behaviour which serve to protect individuals and the community as a whole. When all members of the community share and practice these traits, a safer living and learning environment is created for the good and benefit of all. This also eases undue anxieties and creates a happy atmosphere. We therefore appreciate and are extremely grateful for the positive participation of every member of the community in combating crime in whatever way possible\n\nIt is my fervent hope that you will find this departmental website, not only informative, but also useful in answering any questions you might have and explaining our functions and duties and what you, as our customers should and can expect from us. We believe it is your entitlement to expect quality delivery of service from us. This is what we always strive to do. We are always committed to go the extra mile. Good day."
  },
    {
      title: "Mission Statement",
      content: "The mission of the Security Department is to provide a safe and secure environment to enhance the well-being of staff, students and visitors and to protect all university and personal property. The department is committed to providing a safe living and learning environment in which all members of the community can achieve their goals and objectives"
    },
    {
      title: "Goals and Responsibilities",
      content:"The department meets the challenges enshrined in our mission statement by enforcing the values and regulations set forth by the university policy makers and by taking all responsible steps to maintain order, keeping the peace, safeguarding life and property and preventing or detecting crime. To fulfill these responsibilities the department provides 24 hour a day, 7 days a week and 365 days a year security services. Using a combination of foot, bicycle and motorcycle patrols throughout all shifts, members of the department are continuously visible on campus to act as a de terrent to criminal activity. Our staff is also visible at entry/exit points and at strategic points and vital installations on campus.Members of the Security Department, apart from dealing with crime, also deal with issues like fire prevention and control, and any other imaginable emergencies and threats to campus life. We also provide crowd control duties and enforce traffic and parking regulations. We are also tasked with liaison with national law enforcement agencies whenever the need arises."
    }
  ];
  ngOnInit() {
  }
  async presentModal(info) {
    const modal = await this.modalController.create({
      component: ModalpagePage,
      componentProps: { info: info }
    });
    // modal.onDidDismiss().then(data=>{
    //   this.infoList[index]=data.data
    // })
    return await modal.present();
  }



}
