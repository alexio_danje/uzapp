import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DirectoryService } from 'src/app/services/directory.service';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';

@Component({
  selector: 'app-security',
  templateUrl: './security.page.html',
  styleUrls: ['./security.page.scss'],
})
export class SecurityPage implements OnInit {

 
  constructor(
    private router: Router, ) {
     }

  ngOnInit() {

  }

  contactsecurity() {
    
      this.router.navigateByUrl('contactsecurity');
  
  }
  generalinfo() {
    this.router.navigateByUrl('generalinfo');
  }
  crimeprevention() {
    this.router.navigateByUrl('crimeprevention');
  }
  notices() {
    this.router.navigateByUrl('notices');
  }
}

