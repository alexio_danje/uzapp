import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SportService } from 'src/app/services/sport.service';
import { NagivateDataService } from 'src/app/services/nagivate-data.service';
import { Platform, NavController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-sport',
  templateUrl: './sport.page.html',
  styleUrls: ['./sport.page.scss'],
})
export class SportPage implements OnInit {

  users = []; 
  sports: any;

  constructor(private sportService: SportService,
    private navigateData: NagivateDataService,
    private plt: Platform,
    private navCtrl: NavController,
    private router: Router,
    private toastController:ToastController,) { }

    ngOnInit() {
      this.plt.ready().then(() => {
        this.loadData(true);
      })
    }

  loadData(refresh = false, refresher?) { 
    this.sportService.getSports(refresh).subscribe(res => {
      this.users = res;
      if (refresher) {
        refresher.target.complete(); 
      }
    })
  }
 

moveTabs() {
  this.navigateData.setParamData(this.users);
    this.router.navigateByUrl('tabs');
  }

}
