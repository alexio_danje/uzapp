import { DrugService } from './../../services/drug.service';
import { Platform, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TipdetailPage } from '../tipdetail/tipdetail.page';

@Component({
  selector: 'app-clinic',
  templateUrl: './clinic.page.html',
  styleUrls: ['./clinic.page.scss'],
})
export class ClinicPage implements OnInit {

  medicine: any;
  healthtips: any;

  constructor(private router: Router,
              private plt: Platform,
              private modalCtrl: ModalController,
              private drugService: DrugService,) { }

  ngOnInit() {
    this.plt.ready().then(() => {
      this.drugslist(true);
      this.loadData();
    })
  }


  loadData() {
    this.drugService.getOfflineTips().subscribe(res => {
      this.healthtips = res;
      console.log(this.healthtips)
    });
  }

  async tipDetails(tip) {
    const modal = await this.modalCtrl.create({
      component: TipdetailPage,
      componentProps: {'tip': tip},
    });
    return await modal.present();
  }


  ambulance(){
    this.router.navigateByUrl('ambulance');
  }

  drugs(){
    this.router.navigateByUrl('drugsearch');
  }

  drugslist(refresh = false, refresher?){
    this.drugService.getDrugs(refresh).subscribe(resonse =>{ 
      this.medicine = resonse;
      console.log('drugs', this.medicine);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

}
