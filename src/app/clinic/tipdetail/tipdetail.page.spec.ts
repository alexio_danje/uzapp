import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TipdetailPage } from './tipdetail.page';

describe('TipdetailPage', () => {
  let component: TipdetailPage;
  let fixture: ComponentFixture<TipdetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TipdetailPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TipdetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
