import { NavParams, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tipdetail',
  templateUrl: './tipdetail.page.html',
  styleUrls: ['./tipdetail.page.scss'],
})
export class TipdetailPage implements OnInit {

  tip: any;

  constructor(private modalController: ModalController,
    private navParams: NavParams,) { }


  ngOnInit() {
    this.tip = this.navParams.get('tip');
    console.log('tip', this.tip);
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }


}
