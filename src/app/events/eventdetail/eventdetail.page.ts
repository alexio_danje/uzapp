import { ModalController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-eventdetail',
  templateUrl: './eventdetail.page.html',
  styleUrls: ['./eventdetail.page.scss'],
})
export class EventdetailPage implements OnInit {

  event: any;

  constructor(private modalController: ModalController,
    private socialSharing: SocialSharing,
    private navParams: NavParams,) { }


  ngOnInit() {
    this.event = this.navParams.get('event');
    
    console.log('event', this.event);
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }


   // share(message, subject, file, url)
   share(title, content, venue, custom_venue){

    // console.log('http://uzbuddie.pythonanywhere.com',image);
    // 'http://uzbuddie.pythonanywhere.com'
  
    this.socialSharing.share(title + content , content, venue+" "+custom_venue).then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });
  }

}
