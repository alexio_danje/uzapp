import { Platform } from '@ionic/angular';
import { HomeService } from './../services/home.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.page.html',
  styleUrls: ['./faqs.page.scss'],
})
export class FaqsPage implements OnInit {

  faqs: any;
  isname: any;

  constructor(private homeService: HomeService,
              private plt: Platform,
              ) { }

  ngOnInit() {
    this.plt.ready().then(() => {
      this.faqlist();
    })
  }

  faqlist(){
    this.homeService.getOfflineFaqs().subscribe(resonse =>{ 
      this.faqs = resonse;
      console.log('faqs', this.faqs);
    });
  }


  name(id:any){
    this.isname = id;

 }


}
