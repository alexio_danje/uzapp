import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.page.html',
  styleUrls: ['./bank.page.scss'],
})
export class BankPage implements OnInit {

  constructor(private socialSharing:SocialSharing) { }

  ngOnInit() {
  }

  share(one,two,three) {
    // 'http://uzbuddie.pythonanywhere.com'
    
    this.socialSharing.share(one,two,three).then(() => {

    }).catch(() => {
     
    });


  }

}
