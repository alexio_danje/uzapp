import { Component, OnInit } from '@angular/core';
import { NagivateDataService } from './../../services/nagivate-data.service';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {

  order: any;
  realDate: any;

  constructor(private navigateData: NagivateDataService,private socialSharing: SocialSharing) { }

  ngOnInit() {
    this.order = this.navigateData.getParamData();
    console.log('order', this.order);
    this.realDate = this.order['timestamp'];
  }


  share(name,price,timestamp, restaurant){
  
    this.socialSharing.share(name , price, " The restaurant name is " + restaurant, " Posted on " + timestamp).then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });


}

}
