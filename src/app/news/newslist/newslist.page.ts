import { Component, OnInit } from '@angular/core';
import { NewsnoticesService } from '../../services/newsnotices.service';
import { Router } from '@angular/router';
import { NagivateDataService } from '../../services/nagivate-data.service';

@Component({
  selector: 'app-newslist',
  templateUrl: './newslist.page.html',
  styleUrls: ['./newslist.page.scss'],
})
export class NewslistPage implements OnInit {

  news: any;

  constructor(public newsnoticeService: NewsnoticesService,
              public router: Router,
              public navData: NagivateDataService,
                ) { }

  ngOnInit() {
    this.newslist();
  }

  newslist(){
    this.newsnoticeService.getOfflineNews().subscribe(resonse =>{ 
      this.news = resonse;
      console.log('news', this.news);
    });
  }

  newsDetail(news){
    this.navData.setParamData(news);
    this.router.navigateByUrl('newsdetail');
  }

}
