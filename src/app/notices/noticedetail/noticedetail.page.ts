import { NavParams, ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-noticedetail',
  templateUrl: './noticedetail.page.html',
  styleUrls: ['./noticedetail.page.scss'],
})
export class NoticedetailPage implements OnInit {

  notice: any;

  constructor(private modalController: ModalController,
    private socialSharing: SocialSharing,
    private navParams: NavParams,) { }


  ngOnInit() {
    this.notice = this.navParams.get('notice');
    console.log('notice', this.notice);
  }

  async closeModal() {
    const onClosedData: string = "Wrapped Up!";
    await this.modalController.dismiss(onClosedData);
  }


   // share(message, subject, file, url)
   share(title,content,image, image1){

    console.log('http://uzbuddie.pythonanywhere.com',image);
    // 'http://uzbuddie.pythonanywhere.com'
  
    this.socialSharing.share(title + content , content, "http://uzbuddie.pythonanywhere.com"+image, "http://uzbuddie.pythonanywhere.com"+image1).then(() => {
      console.log("shareSheetShare: Success");
    }).catch(() => {
      console.error("shareSheetShare: failed");
    });
  }

}
