import { Platform } from '@ionic/angular';
// import { NoticedetailPage } from './../../../../src1/app/notices/noticedetail/noticedetail.page';
import { ModalController } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { NewsnoticesService } from 'src/app/services/newsnotices.service';
import { NoticedetailPage } from '../noticedetail/noticedetail.page';

@Component({
  selector: 'app-notices', 
  templateUrl: './notices.page.html',
  styleUrls: ['./notices.page.scss'],
})
export class NoticesPage implements OnInit {

  notice: any;

  constructor(private modalCtrl: ModalController,
              private newsnoticeService: NewsnoticesService,
              private plt: Platform,
              ) { }

              ngOnInit() {
                this.plt.ready().then(() => {
                  this.loadData();
                })
              }


  loadData() {
    this.newsnoticeService.getOfflineNotices().subscribe(res => {
      this.notice = res;
      console.log(this.notice)
    });
  }

  async noticeDetails(notice) {
    const modal = await this.modalCtrl.create({
      component: NoticedetailPage,
      componentProps: {'notice': notice},
    });
    return await modal.present();
  }

}
