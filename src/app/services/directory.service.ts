import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { OfflineManagerService } from './offline-manager.service';
import { NetworkService, ConnectionStatus } from './network.service';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';

import * as jwt_decode from 'jwt-decode';


const API_STORAGE_KEY = 'specialkey';
// const API_URL = 'http://127.0.0.1:8000/api';
const API_URL = 'http://uzbuddie.pythonanywhere.com/api';


@Injectable({
  providedIn: 'root'
})
export class DirectoryService {

  constructor(private storage: Storage, 
    private http: HttpClient, 
    private networkService: NetworkService, 
    private offlineManager: OfflineManagerService,) {

     }

     private setLocalData(key, data) {
      this.storage.set(`${API_STORAGE_KEY}-${key}`, data);
    }
  
    // Get cached API result
    private getLocalData(key) {
      console.log('return local data!');
      return this.storage.get(`${API_STORAGE_KEY}-${key}`);
    }
  
  
  getFaculties(forceRefresh: boolean = false): Observable<any> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
      return from(this.getLocalData('faculties'));
  
    } else {
      // let page = Math.floor(Math.random() * Math.floor(6));

      return this.http.get(`${API_URL}/directories/faculties`).pipe(
        map(res => res),
        tap(res => {
          console.log('returns real live API data');
          this.setLocalData('faculties', res);
          console.log(res);
          console.log('offline data', this.getLocalData('faculties'));
        })
      )
    }
}

getFacultiesOffline(){
 
    return from(this.getLocalData('faculties'));

}

getDepartments(id, data): Observable<any> {
  // id = id - 1;
  let url = `${API_URL}/directories/departments/?faculty_id=` + id;

  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
    // return from(this.offlineManager.storeRequest(url, 'GET', data));
    return from(this.getLocalData('departments'+id));
  } else {
    return this.http.get(url, data).pipe(
      map(res => res),
      tap(res => {
        console.log('returns real live API data');
        this.setLocalData('departments'+id, res);
        console.log(res);
        console.log('offline data', this.getLocalData('departments'+id));
        // throw new Error(err);
        // catchError(err => {
        //   this.offlineManager.storeRequest(url, 'GET', data);
        //   throw new Error(err);
        // })
      })
    );
  }
}
  getDepartmentsOffline(id) {
    return from(this.getLocalData('departments'+id));
  }
  
getDepartments1(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('departments'));

  } else {
    // let page = Math.floor(Math.random() * Math.floor(6));

    return this.http.get(`${API_URL}/directories/departments`).pipe(
      map(res => res),
      tap(res => {
        console.log('returns real live API data'); 
        this.setLocalData('departments', res);
        console.log(res);
        console.log('offline data', this.getLocalData('departments'));
      })
    )
  }
}
  getDepartmentsOffline1() {
    return from(this.getLocalData('departments'));
}
getNumbers(id, data): Observable<any> {
  // id = id - 1;
  let url = `${API_URL}/directories/?department_id=` + id;

  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
    // return from(this.offlineManager.storeRequest(url, 'GET', data));
    return from(this.getLocalData('phones'+id));
  } else {
    return this.http.get(url, data).pipe(
      map(res => res),
      tap(res => {
        console.log('returns real live API data');
        this.setLocalData('phones'+id, res);
        console.log(res);
        console.log('offline data', this.getLocalData('phones'+id));
        // throw new Error(err);
        // catchError(err => {
        //   this.offlineManager.storeRequest(url, 'GET', data);
        //   throw new Error(err);
        // })
      })
    );
  }
}
getNumbersOffline(id) {
  return from(this.getLocalData('phones'+id));
}
  
getNumbers1(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('phones'));

  } else {
    // let page = Math.floor(Math.random() * Math.floor(6));

    return this.http.get(`${API_URL}/directories/`).pipe(
      map(res => res),
      tap(res => {
        console.log('returns real live API data');
        this.setLocalData('phones', res);
        console.log(res);
        console.log('offline data', this.getLocalData('phones'));
      })
    )
  }
}

getNumbersOffline1() {
  return from(this.getLocalData('phones'));
}
getSecurity(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('security'));

  } else {
    // let page = Math.floor(Math.random() * Math.floor(6));

    return this.http.get(`${API_URL}/security/`).pipe(
      map(res => res),
      tap(res => {
        console.log('returns real live API data');
        this.setLocalData('security', res);
        console.log(res);
        console.log('offline data', this.getLocalData('security'));
      })
    )
  }
}

getSecurityOffline(){

  return from(this.getLocalData('security'));

}
}
