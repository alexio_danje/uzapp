import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { OfflineManagerService } from './offline-manager.service';
import { NetworkService, ConnectionStatus } from './network.service';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';

import * as jwt_decode from 'jwt-decode';


const API_STORAGE_KEY = 'specialkey';
// const API_URL = 'http://127.0.0.1:8000/api';
const API_URL = 'http://uzbuddie.pythonanywhere.com/api';

@Injectable({
  providedIn: 'root'
})
export class HostelService {

  constructor(private storage: Storage, 
    private http: HttpClient, 
    private networkService: NetworkService, 
    private offlineManager: OfflineManagerService,) {

     }

     private setLocalData(key, data) {
      this.storage.set(`${API_STORAGE_KEY}-${key}`, data);
    }
  
    // Get cached API result
    private getLocalData(key) {
      return this.storage.get(`${API_STORAGE_KEY}-${key}`);
    }

  getHostels(forceRefresh: boolean = false): Observable<any> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
      return from(this.getLocalData('hostels'));
  
    } else {

      return this.http.get(`${API_URL}/ResidenceHalls`).pipe(
        map(res => res),
        tap(res => {
          this.setLocalData('hostels', res);
        
        })
      )
    }
}

getHostelsOffline(){
 
    return from(this.getLocalData('hostels'));

}


getWardens(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('wardens'));

  } else {

    return this.http.get(`${API_URL}/ResidenceHalls/wardens`).pipe(
      map(res => res),
      tap(res => {
        this.setLocalData('wardens', res);
      
      })
    )
  }
}

getWardensOffline(){

  return from(this.getLocalData('wardens'));

}
  

getJanitors(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('janitors'));

  } else {

    return this.http.get(`${API_URL}/ResidenceHalls/janitors`).pipe(
      map(res => res),
      tap(res => {
        this.setLocalData('janitors', res);
      
      })
    )
  }
}

getJanitorsOffline(){

  return from(this.getLocalData('janitors'));

}
  

getCommities(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('commities'));

  } else {

    return this.http.get(`${API_URL}/ResidenceHalls/commities`).pipe(
      map(res => res),
      tap(res => {
        this.setLocalData('commities', res);
      
      })
    )
  }
}

getCommitiesOffline(){

  return from(this.getLocalData('commities'));

}
}
