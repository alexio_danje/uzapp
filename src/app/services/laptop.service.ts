import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { NetworkService, ConnectionStatus } from './network.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';

const API_STORAGE_KEY = 'specialkey';
// const API_URL = 'http://127.0.0.1:8000/api';
const API_URL = 'http://uzbuddie.pythonanywhere.com/api';
@Injectable({
  providedIn: 'root'
})
export class LaptopService {

  constructor(private storage: Storage, 
    private http: HttpClient, 
    private networkService: NetworkService,       
    ) { }

          // Save result of API requests
private setLocalData(key, data) {
this.storage.set(`${API_STORAGE_KEY}-${key}`, data);
}

// Get cached API result
private getLocalData(key) {
console.log('return local data!');
return this.storage.get(`${API_STORAGE_KEY}-${key}`);
}


getLaptop(id): Observable<any> {
  // id = id - 1;
  let url = `${API_URL}/laptops_register/?user_id=` + id;

  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline) {
    // return from(this.offlineManager.storeRequest(url, 'GET', data));
    return from(this.getLocalData('laptop'));
  } else {
    return this.http.get(url).pipe(
      map(res => res),
      tap(res => {
        console.log('returns real live API data');
        this.setLocalData('laptop', res);
        console.log(res);
        console.log('offline data', this.getLocalData('laptop'));
        // throw new Error(err);
        // catchError(err => {
        //   this.offlineManager.storeRequest(url, 'GET', data);
        //   throw new Error(err);
        // })
      })
    );
  }
}
  getLaptopOffline() {
    return from(this.getLocalData('laptop'));
}
}
