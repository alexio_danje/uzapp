import { TestBed } from '@angular/core/testing';

import { NewsnoticesService } from './newsnotices.service';

describe('NewsnoticesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NewsnoticesService = TestBed.get(NewsnoticesService);
    expect(service).toBeTruthy();
  });
});
