import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { NetworkService, ConnectionStatus } from './network.service';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';


const API_STORAGE_KEY = 'specialkey';
// const API_URL = 'http://127.0.0.1:8000/api';
const API_URL = 'http://uzbuddie.pythonanywhere.com/api';

@Injectable({
  providedIn: 'root'
})
export class NewsnoticesService {

  
  constructor(private storage: Storage, 
    private http: HttpClient, 
    private networkService: NetworkService,       
    ) { }

          // Save result of API requests
private setLocalData(key, data) {
  this.storage.set(`${API_STORAGE_KEY}-${key}`, data);
}

// Get cached API result
private getLocalData(key) {
  console.log('return local data!');
  return this.storage.get(`${API_STORAGE_KEY}-${key}`);
}


getNews(forceRefresh: boolean = false): Observable<any> {
  if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
    return from(this.getLocalData('news'));

  } else {


    return this.http.get(`${API_URL}/news`).pipe(
      map(res => res),
      tap(res => {
      console.log('returns real live API data');
      this.setLocalData('news', res);
      console.log(res);
      console.log('offline data', this.getLocalData('news'));
      })
    )
  }
}

  getOfflineNews(){
    return from(this.getLocalData('news'));
  }


//Notices
  getNotices(forceRefresh: boolean = false): Observable<any> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
      return from(this.getLocalData('notices'));
  
    } else {
  
  
      return this.http.get(`${API_URL}/notices`).pipe(
        map(res => res),
        tap(res => {
        console.log('returns real live API data');
        this.setLocalData('notices', res);
        console.log(res);
        console.log('offline data', this.getLocalData('notices'));
        })
      )
    }
  }
  
    getOfflineNotices(){
      return from(this.getLocalData('notices'));
    }

}
