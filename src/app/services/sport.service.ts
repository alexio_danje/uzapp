import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Observable, from } from 'rxjs';
import { OfflineManagerService } from './offline-manager.service';
import { NetworkService, ConnectionStatus } from './network.service';
import { HttpClient } from '@angular/common/http';
import { map, tap, catchError } from 'rxjs/operators';

import * as jwt_decode from 'jwt-decode';


const API_STORAGE_KEY = 'specialkey';
// const API_URL = 'http://127.0.0.1:8000/api';
const API_URL = 'http://uzbuddie.pythonanywhere.com/api';

@Injectable({
  providedIn: 'root'
})
export class SportService {

  constructor(private storage: Storage, 
    private http: HttpClient, 
    private networkService: NetworkService, 
    private offlineManager: OfflineManagerService,) {

     }

     private setLocalData(key, data) {
      this.storage.set(`${API_STORAGE_KEY}-${key}`, data);
    }
  
    // Get cached API result
    private getLocalData(key) {
      console.log('return local data!');
      return this.storage.get(`${API_STORAGE_KEY}-${key}`);
    }

  getSports(forceRefresh: boolean = false): Observable<any> {
    if (this.networkService.getCurrentNetworkStatus() == ConnectionStatus.Offline || !forceRefresh) {
      return from(this.getLocalData('sports'));
  
    } else {
      // let page = Math.floor(Math.random() * Math.floor(6));

      return this.http.get(`${API_URL}/SportsandFacility/sports`).pipe(
        map(res => res),
        tap(res => {
          console.log('returns real live API data');
          this.setLocalData('sports', res);
          console.log(res);
          console.log('offline data', this.getLocalData('sports'));
        })
      )
    }
  }

  getSportsOffline(){
 
    return from(this.getLocalData('sports'));

}
}
