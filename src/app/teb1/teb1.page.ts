

import { ConnectionStatus } from './../services/network.service';
import { FoodService } from './../services/food.service';
import { Platform, IonSlides } from '@ionic/angular';
import { UserService } from './../services/user.service';
// import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
// import { AuthService } from '../auth/auth.service';
// import { ApiService } from '../services/api.service';
import { NewsnoticesService } from '../services/newsnotices.service';
import { HomeService } from '../services/home.service';
import { LaptopService } from '../services/laptop.service';
import { EventsService } from '../services/events.service';
import { DrugService } from '../services/drug.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { OfflineManagerService } from '../services/offline-manager.service';
import { NetworkService } from '../services/network.service';
import { DirectoryService } from '../services/directory.service';
import { HostelService } from '../services/hostel.service';
import { MapService } from '../services/map.service';


@Component({
  selector: 'app-teb1',
  templateUrl: './teb1.page.html',
  styleUrls: ['./teb1.page.scss'],
})
export class Teb1Page implements OnInit {

  
  slideOpts = {
    autoplay:true,
    initialSlide: 0,
    speed: 1000
  };
 
  token: any;
  users :any;
  response: any;
  response1: any;
  status: any;
  news: any;
  notices: any;
  meals: any;
  trending = [];
  final = [];

  home: any;
  banner: any;
  banner1: any;
  id:any
  laptop: any;

  // home: any;
  tip: any;
  event: any;
  faq: any;
  place: any;
  faculty: any;
  security: any;
  // id: any;
  // token: any;

  constructor(
              public router: Router, 
             private platform: Platform,
              public userService: UserService,
              private plt: Platform,
    public homeService: HomeService,
    private mapService: MapService,
              public foodService: FoodService,
              public newsnoticeService: NewsnoticesService,
              private laptopService: LaptopService,
              private drugService: DrugService,
              private eventService: EventsService,

              private offlineManager: OfflineManagerService,
              private networkService: NetworkService,
              private directoryService: DirectoryService,
              private hostelService: HostelService,
              ) { 

                const token = JSON.parse(localStorage.getItem('token'));
                this.token = token;
                // console.log('token again', this.token);

               if(this.token!=null){
                this.allusers();
               }
   }

  ngOnInit() {
    this.plt.ready().then(() => {
      this.newslist(true);
      this.mealslist(true);
      this.homelist();
      this.noticeslist(true);
      this.lapers();
      this.initializeApp();
    })
    
  }
  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
      this.homelist();
      this.placelist(true);
      this.tiplist(true);
      this.eventlist(true);
      this.faqlist(true);
      // this.facultylist(true);
      // this.securityNumbers(true);
      // this.departmentlist(true);
      // this.phonelist(true);
      this.hostellist(true);

      this.janitorlist(true);
      this.wardenlist(true);
      this.commitielist(true);
 

      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log('we are online, run checkForEvents...');
          this.offlineManager.checkForEvents().subscribe();
        }
      })
    });
  }
  map(){
    this.router.navigateByUrl('/map');
  }

  hey = "data";

  allusers(){
    this.userService.allusers(this.token, this.hey).subscribe(resonse =>{ 
      this.users = resonse;
      // console.log('users', this.users);
      localStorage.setItem('user', JSON.stringify(this.users[0]));
      // console.log('me', this.users[0]);
      this.response = this.users[0]['username']
      this.status = this.users[0]['is_staff']

      this.response1 = this.users[0]['is_staff']
      localStorage.setItem('status', JSON.stringify(this.response1));

      localStorage.setItem('username', JSON.stringify(this.response));
      
    
    });
  }



  noticeslist(refresh = false, refresher?){
    this.newsnoticeService.getNotices(refresh).subscribe(resonse =>{ 
      // this.notices = resonse;
      // console.log('notices', this.notices);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

  newslist(refresh = false, refresher?){
    this.newsnoticeService.getNews(refresh).subscribe(resonse =>{ 
      this.news = resonse;
      this.home = resonse;
      // console.log('news', this.news);
      // console.log('news', this.home);
      // for (let i = this.home.length-1; i > this.home.length-4; i--) {
      //   this.trending = this.home[i];
      //   console.log('trending', this.trending);
      //   this.final.push(this.home[i]);
      //   console.log('final', this.final);
      // }

      for (let i = 0; i < 3; i++) {
        this.trending = this.home[i];
        console.log('trending', this.trending);
        this.final.push(this.home[i]);
        console.log('final', this.final);
      }
      console.log('length', this.home.length);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

  homelist(){
    this.homeService.getOfflineHome().subscribe(resonse =>{  
      this.home = resonse;
    // console.log('home', this.home);
      this.banner = resonse[0]['banner'];
      this.banner1 = resonse[0]['banner1'];
      // for (let i = this.home.length; i > this.home.length-3; i--) {
      //   this.trending = this.home[i];
      //   console.log('trending', this.trending);
      // }
      // console.log('length', this.home.length);
      // console.log('banner', this.banner);
      // console.log('banner1', this.banner1);
      // if (refresher) {
      //   refresher.target.complete();
      // }
    });
  }

  mealslist(refresh = false, refresher?){
    this.foodService.getMeals(refresh).subscribe(resonse =>{ 
      // this.meals = resonse;
      // console.log('meals', this.meals);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

  lapers() {
    const id = JSON.parse(localStorage.getItem('user_id'));
    this.id = id;
    console.log("usewr id", this.id);
    this.laptopService.getLaptop(this.id).subscribe(res => { 
      this.laptop = res; 
      console.log('laptop',this.laptop) 
    }) 

  }

//   backToWelcome(){
//     this.router.navigateByUrl('login');    

// }

//   logout(){
//     localStorage.clear();
//     setTimeout(() => this.backToWelcome(), 500);
// }

// homelist(refresh = false, refresher?){
//   this.homeService.getHome(refresh).subscribe(resonse =>{ 
//     // this.home = resonse;
//     // console.log('home', this.home);
//     if (refresher) {
//       refresher.target.complete();
//     }
//   });
// }

placelist(refresh = false, refresher?){
  this.mapService.getCords(refresh).subscribe(resonse =>{ 
    this.place = resonse;
    console.log('places', this.place);
    if (refresher) {
      refresher.target.complete();
    }
  });
}


tiplist(refresh = false, refresher?){
  this.drugService.getHealthtips(refresh).subscribe(resonse =>{ 
    // this.tip = resonse;
    // console.log('tips', this.tip);
    if (refresher) {
      refresher.target.complete();
    }
  });
}

eventlist(refresh = false, refresher?){
  this.eventService.getEvents(refresh).subscribe(resonse =>{ 
    // this.event = resonse;
    // console.log('events', this.event);
    if (refresher) {
      refresher.target.complete();
    }
  });
}

faqlist(refresh = false, refresher?){
  this.homeService.getFaqs(refresh).subscribe(resonse =>{ 
    // this.faq = resonse;
    // console.log('faqs', this.faq);
    if (refresher) {
      refresher.target.complete();
    }
  });
}

// facultylist(refresh = false, refresher?){
//   this.directoryService.getFaculties(refresh).subscribe(resonse =>{ 
//     // this.faq = resonse;
//     // console.log('faqs', this.faq);
//     if (refresher) {
//       refresher.target.complete();
//     }
//   });
// }


// departmentlist(refresh = false, refresher?){
//   this.directoryService.getDepartments1(refresh).subscribe(resonse =>{ 
//     if (refresher) {
//       refresher.target.complete();
//     }
//   });
// }

// phonelist(refresh = false, refresher?){
//   this.directoryService.getNumbers1(refresh).subscribe(resonse =>{ 
//     if (refresher) {
//       refresher.target.complete();
//     }
//   });
// }
// securityNumbers(refresh = false, refresher?){
//   this.directoryService.getSecurity(refresh).subscribe(resonse =>{ 
//     // this.faq = resonse;
//     // console.log('faqs', this.faq);
//     if (refresher) {
//       refresher.target.complete();
//     }
//   });
// }
hostellist(refresh = false, refresher?){
  this.hostelService.getHostels(refresh).subscribe(resonse =>{ 
    // this.faq = resonse;
    // console.log('faqs', this.faq);
    if (refresher) {
      refresher.target.complete();
    }
  });
}

janitorlist(refresh = false, refresher?){
  this.hostelService.getJanitors(refresh).subscribe(resonse => {
    if (refresher) {
      refresher.target.complete();
    }
  });
}

wardenlist(refresh = false, refresher?){
  this.hostelService.getWardens(refresh).subscribe(resonse => {
    if (refresher) {
      refresher.target.complete();
    }
  });
}

commitielist(refresh = false, refresher?){
  this.hostelService.getCommities(refresh).subscribe(resonse => {
    if (refresher) {
      refresher.target.complete();
    }
  });
}

  viewnews(){
    this.router.navigateByUrl('newslist');
  }

  events(){
    // this.router.navigateByUrl('events');
  }

  sports(){
     this.router.navigateByUrl('sport');
  }

  finance(){
    // this.router.navigateByUrl('bank');
  }

  hostels(){
    // this.router.navigateByUrl('hostels');
  }
  phone(){
    this.router.navigateByUrl('directory');
  }
  Notices() {
    this.router.navigateByUrl('notices');
  }

  

  backToWelcome(){
    this.router.navigateByUrl('login');    

}

  logout(){
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 500);
}

}

