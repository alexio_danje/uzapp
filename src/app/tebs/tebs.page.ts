import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { HomeService } from '../services/home.service';
import { MapService } from '../services/map.service';
import { DrugService } from '../services/drug.service';
import { EventsService } from '../services/events.service';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Router } from '@angular/router';
import { OfflineManagerService } from '../services/offline-manager.service';
import { NetworkService, ConnectionStatus } from '../services/network.service';
import { DirectoryService } from '../services/directory.service';
import { HostelService } from '../services/hostel.service';

@Component({
  selector: 'app-tebs',
  templateUrl: './tebs.page.html',
  styleUrls: ['./tebs.page.scss'],
})
export class TebsPage implements OnInit {

  home: any;
  tip: any;
  event: any;
  faq: any;
  place: any;
  faculty: any;
  security: any;
  id: any;
  token: any;

  constructor(
    public router: Router,
    private platform: Platform,
    // public homeService: HomeService,
    // private mapService: MapService,

    // private drugService: DrugService,
    // private eventService: EventsService,

    private offlineManager: OfflineManagerService,
    private networkService: NetworkService,
    private directoryService: DirectoryService,
    // private hostelService: HostelService,
  ) {

    const token = JSON.parse(localStorage.getItem('token'));
    this.token = token;
    // console.log('token again', this.token);

    if (this.token != null) {
    }
     this.initializeApp();
  }

  ngOnInit() {
    // this.plt.ready().then(() => {
    //   this.initializeApp();
    // })

  }
  initializeApp() {
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      // this.splashScreen.hide();
      // this.placelist(true);
      // this.tiplist(true);
      // this.eventlist(true);
      // this.faqlist(true);
      this.facultylist(true);
      this.securityNumbers(true);
      this.departmentlist(true);
      this.phonelist(true);
      // this.hostellist(true);

      // this.janitorlist(true);
      // this.wardenlist(true);
      // this.commitielist(true);


      this.networkService.onNetworkChange().subscribe((status: ConnectionStatus) => {
        if (status == ConnectionStatus.Online) {
          console.log('we are online, run checkForEvents...');
          this.offlineManager.checkForEvents().subscribe();
        }
      })
    });
  }
  // map(){
  // this.router.navigateByUrl('/map');
  // }

  // hey = "data";



  // placelist(refresh = false, refresher?){
  // this.mapService.getCords(refresh).subscribe(resonse =>{ 
  // this.place = resonse;
  // console.log('places', this.place);
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }


  // tiplist(refresh = false, refresher?){
  // this.drugService.getHealthtips(refresh).subscribe(resonse =>{ 
  // // this.tip = resonse;
  // // console.log('tips', this.tip);
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }

  // eventlist(refresh = false, refresher?){
  // this.eventService.getEvents(refresh).subscribe(resonse =>{ 
  // // this.event = resonse;
  // // console.log('events', this.event);
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }

  // faqlist(refresh = false, refresher?) {
  //   this.homeService.getFaqs(refresh).subscribe(resonse => {
  //     // this.faq = resonse;
  //     // console.log('faqs', this.faq);
  //     if (refresher) {
  //       refresher.target.complete();
  //     }
  //   });
  // }

  facultylist(refresh = false, refresher?) {
    this.directoryService.getFaculties(refresh).subscribe(resonse => {
      // this.faq = resonse;
      // console.log('faqs', this.faq);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }


  departmentlist(refresh = false, refresher?) {
    this.directoryService.getDepartments1(refresh).subscribe(resonse => {
      if (refresher) {
        refresher.target.complete();
      }
    });
  }

  phonelist(refresh = false, refresher?) {
    this.directoryService.getNumbers1(refresh).subscribe(resonse => {
      if (refresher) {
        refresher.target.complete();
      }
    });
  }
  securityNumbers(refresh = false, refresher?) {
    this.directoryService.getSecurity(refresh).subscribe(resonse => {
      // this.faq = resonse;
      // console.log('faqs', this.faq);
      if (refresher) {
        refresher.target.complete();
      }
    });
  }
  // hostellist(refresh = false, refresher?){
  // this.hostelService.getHostels(refresh).subscribe(resonse =>{ 
  // // this.faq = resonse;
  // // console.log('faqs', this.faq);
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }

  // janitorlist(refresh = false, refresher?){
  // this.hostelService.getJanitors(refresh).subscribe(resonse => {
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }

  // wardenlist(refresh = false, refresher?){
  // this.hostelService.getWardens(refresh).subscribe(resonse => {
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }

  // commitielist(refresh = false, refresher?){
  // this.hostelService.getCommities(refresh).subscribe(resonse => {
  // if (refresher) {
  // refresher.target.complete();
  // }
  // });
  // }

  viewnews() {
    this.router.navigateByUrl('newslist');
  }

  events() {
    // this.router.navigateByUrl('events');
  }

  sports() {
    this.router.navigateByUrl('sport');
  }

  finance() {
    // this.router.navigateByUrl('bank');
  }

  hostels() {
    // this.router.navigateByUrl('hostels');
  }
  phone() {
    this.router.navigateByUrl('directory');
  }
  Notices() {
    this.router.navigateByUrl('notices');
  }



  backToWelcome() {
    this.router.navigateByUrl('login');

  }

  logout() {
    localStorage.clear();
    setTimeout(() => this.backToWelcome(), 500);
  }



}